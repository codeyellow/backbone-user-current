import _ from 'underscore';
import Backbone from 'backbone';

export default function (MUser) {
    return class MUserCurrent extends MUser {
        isLoggedIn() {
            return this.get('id') !== null;
        }
        /**
         * Set a new current user that is logged in.
         * If no user is given, the defaults will be used.
         */
        fresh(payload) {
            return this.clear({ silent: true }).set(payload || this.defaults());
        }
        /**
         * Login user.
         *
         * @param {Object} data Login data
         * @return {jqXHR} jqXHR
         */
        login(data) {
            const options = {
                type: 'POST',
                url: _.result(this, 'loginUrl', `${this.urlRoot}/login`),
                data,
            };

            return Backbone.ajax(options)
            .then((payload) => {
                console.assert(payload, 'Did not receive user object');

                this.fresh(payload);

                this.trigger('after:login:success', payload);
            });
        }
        /**
         * Logout user (might switch back to another user if this was a masqueraded login).
         *
         * @return {jqXHR} jqXHR
         */
        logout() {
            const options = {
                type: 'POST',
                url: _.result(this, 'logoutUrl', `${this.urlRoot}/logout`),
            };

            return Backbone.ajax(options)
            .then((payload) => {
                this.fresh();

                this.trigger('after:logout:success', payload);
            });
        }
        /**
         * Masquerade as another user ("su" to that user)
         *
         * @return {jqXHR} jqXHR
         */
        masquerade(model, data) {
            console.assert(model instanceof MUser, 'A user model should be given.');

            const options = {
                type: 'GET',
                url: _.result(this, 'masqueradeUrl', `${this.urlRoot}/${model.get('id')}/masquerade`),
                error: () => this.trigger('after:masquerade:fail'),
                data,
            };

            return Backbone.ajax(options)
            .then((payload) => {
                this.fresh(payload);

                this.trigger('after:masquerade:success', payload);
            });
        }
    };
}
