# backbone-user-current

Provides a default user model for the current user. It works by extending a given user model, and adding authentication methods to it.

## Usage

```js
import BackboneUserCurrent from 'backbone-user-current';
import MUser from 'model/user';

export default new BackboneUserCurrent(MUser)();
```

Or, extend it:

```js
import backboneUserCurrent from 'backbone-user-current';
import MUser from 'model/user';

class MUserCurrent extends backboneUserCurrent(MUser) {
    // go nuts

    login(data) {
        data = _.extend({ include: 'extension' }, data)

        super.login.call(this, data);
    }
}

export default new MUserCurrent();
```

It is meant to be used as a singleton, so you can easily access the current user's data from other parts in your application. A good location would be `model/user/current.js`.

## Methods

### `isLoggedIn()`

Use this to check if the current user is logged in (wow this makes sense).

### `login()`

Performs a POST request to url root of provided model + `/login`. If it succeeds, `after:login:success` is triggered on the model and the response data will be set on the model. 
You are expected to deal with an unsuccesful login by using the jquery promise it returns. `after:login:success` is useful because you can use this throughout your application to start things after logging in. The error event however, is only relevant on the login page.

### `fresh(user)`

Sets a given `user` object (also works with a model) as the current user. If `user` is not given, the current user will be cleared. Useful if the user if you get the user data from another url (e.g. `api/bootstrap`).

### `logout()`

Performs a POST request to url root of provided model + `/logout`. If this succeeds, it will clear the model and `after:logout:success` is triggered.

### `masquerade(model)`

Tries to login as given user model. It performs a GET request to url root of provided model + `/x/masquerade`, where x is the id of the given user model.

## Events

### `after:login:success` `(payload)`

Triggered after a successful login has occured. Payload is the response given by the sevrer.

### `after:logout:success` `(payload)`

Similar to above.

### `after:masquerade:success` `(payload)`

Similar to above.

## API expectations

The `urlRoot` property of your model will be prepended to all urls. In the examples below it is `api/user`.

- POST `api/user/login` - if success, return the user object.
- POST `api/user/logout` - if logout is successful, return status code in `2xx` range. Otherwise something in the `4xx` range.
- GET `api/user/1/masquerade` - if success, return the user object.

If your API uses other urls, you can change them by extending the model and adding a `loginUrl`, `logoutUrl`, `masqueradeUrl` as a property or function.
