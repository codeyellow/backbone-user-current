(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('underscore'), require('backbone')) :
  typeof define === 'function' && define.amd ? define('backbone-user-current', ['underscore', 'backbone'], factory) :
  (global.backboneUserCurrent = factory(global._,global.Backbone));
}(this, function (_,Backbone) { 'use strict';

  _ = 'default' in _ ? _['default'] : _;
  Backbone = 'default' in Backbone ? Backbone['default'] : Backbone;

  var classCallCheck = function (instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  };

  var createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  var inherits = function (subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  };

  var possibleConstructorReturn = function (self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  };

  function index (MUser) {
      return function (_MUser) {
          inherits(MUserCurrent, _MUser);

          function MUserCurrent() {
              classCallCheck(this, MUserCurrent);
              return possibleConstructorReturn(this, Object.getPrototypeOf(MUserCurrent).apply(this, arguments));
          }

          createClass(MUserCurrent, [{
              key: 'isLoggedIn',
              value: function isLoggedIn() {
                  return this.get('id') !== null;
              }
              /**
               * Set a new current user that is logged in.
               * If no user is given, the defaults will be used.
               */

          }, {
              key: 'fresh',
              value: function fresh(payload) {
                  return this.clear({ silent: true }).set(payload || this.defaults());
              }
              /**
               * Login user.
               *
               * @param {Object} data Login data
               * @return {jqXHR} jqXHR
               */

          }, {
              key: 'login',
              value: function login(data) {
                  var _this2 = this;

                  var options = {
                      type: 'POST',
                      url: _.result(this, 'loginUrl', this.urlRoot + '/login'),
                      data: data
                  };

                  return Backbone.ajax(options).then(function (payload) {
                      console.assert(payload, 'Did not receive user object');

                      _this2.fresh(payload);

                      _this2.trigger('after:login:success', payload);
                  });
              }
              /**
               * Logout user (might switch back to another user if this was a masqueraded login).
               *
               * @return {jqXHR} jqXHR
               */

          }, {
              key: 'logout',
              value: function logout() {
                  var _this3 = this;

                  var options = {
                      type: 'POST',
                      url: _.result(this, 'logoutUrl', this.urlRoot + '/logout')
                  };

                  return Backbone.ajax(options).then(function (payload) {
                      _this3.fresh();

                      _this3.trigger('after:logout:success', payload);
                  });
              }
              /**
               * Masquerade as another user ("su" to that user)
               *
               * @return {jqXHR} jqXHR
               */

          }, {
              key: 'masquerade',
              value: function masquerade(model, data) {
                  var _this4 = this;

                  console.assert(model instanceof MUser, 'A user model should be given.');

                  var options = {
                      type: 'GET',
                      url: _.result(this, 'masqueradeUrl', this.urlRoot + '/' + model.get('id') + '/masquerade'),
                      error: function error() {
                          return _this4.trigger('after:masquerade:fail');
                      },
                      data: data
                  };

                  return Backbone.ajax(options).then(function (payload) {
                      _this4.fresh(payload);

                      _this4.trigger('after:masquerade:success', payload);
                  });
              }
          }]);
          return MUserCurrent;
      }(MUser);
  }

  return index;

}));